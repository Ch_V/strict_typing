"""Module with decorator `@strict_typing` to provide strict
 check of input and output types of functions and methods.
 For more information see `@strict_typing`-decorator documentation.
 """

from .strict_typing import strict_typing
