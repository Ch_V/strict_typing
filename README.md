# STRICT TYPING

A small package to provide strict args, kwargs and return type check on Python3's 
functions and methods. It contains a decorator to strictly check input and output types of functions and methods.
It raises TypeError if any of args, kwargs or return type is wrong according to type annotations.

String and generic annotations are ignored, but you can use tuples of types as union annotation.

May not work on all decorators combinations due to `.__annotations__` nonpropagation.
In combination with `@staticmethod`, `@classmethod` and `@(property).setter` decorators
it should be used as first (bottom) decorator.

You can specify boolean keyword parameter `enabled` ( @strict_typing(enabled=False) )
to disable decorators to prevent extra evaluation cost caused by their usage or any other reason

You can use it, for example, as:

    from strict_typing import strict_typing
    
    SHOULD_STRICT_TYPING_BE_ENABLED = True
        

    @strict_typing
    def my_fun(x: int, y: (int, float)) -> (int, float):
        ...


    @strict_typing(enabled=SHOULD_STRICT_TYPING_BE_ENABLED)
    def my_another_fun(x: int, y: (int, float)) -> (int, float):
        ...
        

    class A:
        @staticmethod
        @strict_typing
        def my_method(x: str):
            ...

        @classmethod
        @strict_typing(enabled=SHOULD_STRICT_TYPING_BE_ENABLED)
        def my_another_method(cls, x: (list, tuple)) -> (int, float):
            ...

---

Experimentally (use it with caution) you can use `@strict_typing` as class decorator
that results in a try to apply it to all class members as first (bottom) decorator (if it is already
applied, maybe even not at first (bottom) place, class-`@strict_typing` decorator does nothing). 
So it could be used with `@staticmethod`, `@classmethod` and `@(property).setter` decorators.

You can use it, for example, as:

    from strict_typing import strict_typing

    SHOULD_STRICT_TYPING_BE_ENABLED = True


    @strict_typing
    clas MyClass:
        ...
    

    @strict_typing(enabled=SHOULD_STRICT_TYPING_BE_ENABLED)
    clas MyAnotherClass:
        ...

(!) When `@strict_typing` is used as class decorator, locally-(inside methods)-defined objects'
`.__qualname__` will work just as `.__name__`.

Due to `inspect.getsource` limitations: \
(!) `@strict-typing` decorator can not be used as class decorator
directly in interactive shell, and \
(!) names of classes decorated with it should be unique
(class overloading may result in unexpected result with `inspect.getsource`:

    import inspect


    class A:
        x = 123
    

    class A:
        x = 222
    

    print(inspect.getsource(A))
    # class A:
    #     x = 123
    print('A.x =', A.x)
    # A.x = 222

).
