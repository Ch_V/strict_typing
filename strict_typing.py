import ast
import functools
import inspect
import textwrap
import typing


def strict_typing(f=None, *, enabled: bool = True):
    """Decorator to strictly check input and output types of functions and methods.
    It raises TypeError if any of args, kwargs or return type is wrong according to type annotations.

    String and generic annotations are ignored, but you can use tuples of types as union annotation.

    May not work on all decorators combinations due to `.__annotations__` nonpropagation.
    In combination with `@staticmethod`, `@classmethod` and `@(property).setter` decorators
    it should be used as first (bottom) decorator.

    You can specify boolean keyword parameter `enabled` ( @strict_typing(enabled=False) )
    to disable decorators to prevent extra evaluation cost caused by their usage or any other reason.

    ---
    Experimentally (use it with caution) you can use `@strict_typing` as class decorator
    that results in a try to apply it to all class members as first (bottom) decorator (if it is already
    applied, maybe even not at first (bottom) place, class-`@strict_typing` decorator does nothing).
    So it could be used with `@staticmethod`, `@classmethod` and `@(property).setter` decorators.

    When `@strict_typing` is used as class decorator, locally-(inside methods)-defined objects'
    `.__qualname__` will work just as `.__name__`.

    Due to `inspect.getsource` limitations `@strict-typing` decorator can not be used as class decorator
    directly in interactive shell, and names of classes decorated with it should be unique
    (class overloading may result in unexpected result with `inspect.getsource`).
    """
    if f:
        return _strict_typing(enabled=enabled)(f)
    else:
        return _strict_typing(enabled=enabled)


def _strict_typing(enabled):
    def _strict_typing_inner(f):
        if not enabled:
            return f

        if not issubclass(type(f), type):  # for functions and methods
            return _func_strict_typing(f)

        else:  # for classes
            return _class_strict_typing(f)

    return _strict_typing_inner


def _func_strict_typing(f):
    """Decorates function or method."""
    if not hasattr(f, '__annotations__'):
        raise TypeError(f'strict_typing decorator can not be applied on {f} (type of {type(f)})')

    annotation_types_to_ignore = (str, typing._GenericAlias)

    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        annotations = f.__annotations__
        sig = inspect.signature(f)
        sig_b = sig.bind(*args, **kwargs)

        for arg, val in sig_b.arguments.items():
            try:
                if isinstance(annotations[arg], annotation_types_to_ignore):
                    continue
                if not isinstance(val, annotations[arg]):
                    raise TypeError(f'STRICT_TYPING: Argument {arg} of {f.__qualname__} '
                                    f'is instance of {type(val)} instead of {annotations[arg]}.')
            except KeyError:
                pass

        for kwarg, val in sig_b.kwargs.items():
            try:
                if isinstance(annotations[kwarg], annotation_types_to_ignore):
                    continue
                if not isinstance(val, annotations[kwarg]):
                    raise TypeError(f'STRICT_TYPING: Argument {kwarg} of {f.__qualname__} '
                                    f'is instance of {type(val)} instead of {annotations[kwarg]}.')
            except KeyError:
                pass

        res = f(*args, **kwargs)
        try:
            if not isinstance(annotations['return'], annotation_types_to_ignore) \
                    and not isinstance(res, annotations['return']):
                raise TypeError(f'STRICT_TYPING: {f.__qualname__} returned type {type(res)} '
                                f'instead of {annotations["return"]}.')
        except KeyError:
            pass

        return res
    return wrapper


def _class_strict_typing(cls):
    """Decorates class."""
    code = inspect.getsource(cls)
    code = _ast(code)
    cls = _apply_code(cls, code)
    return cls


def _ast(code: 'full class definition code string'):
    """Processes code (inserts '@strict-typing' decorator as first (bottom) decorator to all
    nondecorated with it methods and iside-defined classes) using ast-module."""
    code = textwrap.dedent(code)
    c = ast.parse(code)

    for node in ast.walk(c.body[0]):
        if issubclass(type(node), (ast.FunctionDef, ast.AsyncFunctionDef, ast.ClassDef)):
            if ast.Name(id='strict_typing', ctx=ast.Load()) not in node.decorator_list:
                node.decorator_list.append(ast.Name(id='strict_typing', ctx=ast.Load()))

    ast.fix_missing_locations(c)

    # print(ast.dump(c))  # print resulting ast-parsed code

    code = compile(ast.Module(c.body[0].body, type_ignores=[]), 'filename', 'exec')
    return code


def _apply_code(cls, code: "class body code string or code object"):
    """Applies code to class cls as if it is body code of class cls and returns resulting class."""

    # Search for frame strict-typing decorator was called from
    frame = inspect.currentframe().f_back.f_back
    while True:
        frame_old = frame
        frame = frame.f_back
        if frame.f_code.co_filename != frame_old.f_code.co_filename:
            break

    gl = frame.f_globals
    gl.update(frame.f_locals)
    gl.update({'strict_typing': strict_typing})

    attrs = type(cls).__prepare__(cls.__name__, cls.__bases__)
    exec(code, gl, attrs)
    res_cls = type(cls)(cls.__name__, cls.__bases__, attrs)

    _copy_qualname(cls, res_cls)

    return res_cls


def _copy_qualname(src_cls, dst_cls):
    """Copies __qualname__ of members of src_class to members of dst class.
    Don't work for locally-(inside methods)-defined objects.
    """
    for (_, s), (_, d) in zip(inspect.getmembers(src_cls), inspect.getmembers(dst_cls)):
        try:
            d.__qualname__ = s.__qualname__
            _copy_qualname(s, d)
        except (AttributeError, TypeError):
            pass


if __name__ == '__main__':
    raise Exception('`strict_typing` decorator should be imported '
                    'from `strict_typing` module, not applied inside it!')
