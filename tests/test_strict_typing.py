import asyncio
from functools import lru_cache
from typing import Union

import pytest

from strict_typing import strict_typing


def test_enabled():
    """Test of `enabled` boolean kwarg work."""
    @strict_typing
    def f(x: int, y: int) -> int:
        return x + y

    assert f(3, 5) == 8
    with pytest.raises(TypeError):
        f(3.5, 5.5)

    @strict_typing(enabled=False)
    def f(x: int, y: int) -> int:
        return x + y

    assert f(3, 5) == 8
    assert f(3.5, 5.2) == 8.7


    @strict_typing(enabled=True)
    def f(x: int, y: int) -> int:
        return x + y

    assert f(3, 5) == 8
    with pytest.raises(TypeError):
        f(3.5, 5.5)


@strict_typing()
def f0(x: int, y: int) -> int:
    return x + y


@strict_typing
def f1(x: int, y: int) -> int:
    return x + y


@strict_typing
def f2(x, y) -> int:
    return x + y


@strict_typing
def f3(x: float, y):
    return x + y


@strict_typing
def f4(x: 'int', y: 'qwerty') -> 'dict':  # ignores string annotation
    return x + y


class A1:
    @strict_typing
    def __call__(self, x: float, y: int) -> float:
        return x + y

a1 = A1()


class A1_cl:
    @classmethod
    @strict_typing
    def __call__(cls, x: float, y: int) -> float:
        return x + y

a1_cl = A1_cl()


class A1_st:
    @staticmethod
    @strict_typing
    def __call__(x: float, y: int) -> float:
        return x + y

a1_st = A1_st()


class A2:
    @strict_typing
    def __call__(self, x, y: int) -> float:
        return x + y

a2 = A2()


@pytest.mark.parametrize('f, x, y, res', [
    (f0, 1, 2, 3),
    (f0, 1, 2.0, None),
    (f1, 1, 2, 3),
    (f1, 1, 2.0, None),
    (f1, 1.0, 2, None),
    (f1, 1.0, 2.0, None),
    (f2, 1, 2, 3),
    (f2, 1, 2.0, None),
    (f3, 1.4, 2.0, 3.4),
    (f3, 1, 2.0, None),
    (f3, 1.0, 2, 3.0),
    (f4, 1.4, 2.0, 3.4),
    (f4, '1.4', '2.0', '1.42.0'),
    (a1, 2.2, 3, 5.2),
    (a1, 2, 3, None),
    (a1_cl, 2.2, 3, 5.2),
    (a1_cl, 2, 3, None),
    (a1_st, 2.2, 3, 5.2),
    (a1_st, 2, 3, None),
])
def test_args(f, x, y, res):
    """Tests args and return annotations and decorators combinations."""
    if res:
        assert f(x, y) == res
    else:
        with pytest.raises(TypeError):
            f(x, y)


@pytest.mark.parametrize('f, x, y, res', [
    (f1, 1, 2, 3),
    (f1, 1, 2.0, None),
    (f1, 1.0, 2, None),
    (f1, 1.0, 2.0, None),
    (f2, 1, 2, 3),
    (f2, 1, 2.0, None),
    (f3, 1.4, 2.0, 3.4),
    (f3, 1, 2.0, None),
    (f3, 1.0, 2, 3.0),
    (f4, 1.4, 2.0, 3.4),
    (f4, '1.4', '2.0', '1.42.0'),
    (a2, 2, 3, None),
    (a2, 2.0, 3, 5.0),
    (a2, 2.0, 3.0, None),
])
def test_kwargs(f, x, y, res):
    """Tests kwargs and return."""
    if res:
        assert f(y=y, x=x) == res
    else:
        with pytest.raises(TypeError):
            f(y=y, x=x)


class Pr:
    @property
    def x(self):
        return self._x

    @x.setter
    @strict_typing
    def x(self, x: int):
        self._x = x

pr = Pr()


@pytest.mark.parametrize('x, res', [
    (1, 1),
    (1.0, None),
])
def test_property(x, res):
    """Test with property."""
    if res:
        pr.x = x
        assert pr.x == res
    else:
        with pytest.raises(TypeError):
            pr.x = x


@strict_typing
def g1(x: Union[int, float]):  # ignores generic annotation
    return x * 3


@strict_typing
def g2(x: (int, float)):
    return x * 3


@lru_cache
@strict_typing
def l1(x) -> int:
    return x


@strict_typing
@lru_cache
def l2(x) -> int:
    return x


@pytest.mark.parametrize('f, x, res', [
    (g1, 's', 'sss'),
    (g2, '1', None),
    (g2, 3, 9),
    (g2, 3.0, 9.0),
    (l1, 3.0, None),
    (l1, 3, 3),
    (l2, 3.0, None),
    (l2, 3, 3),
])
def test_generic_union_lru(f, x, res):
    """Test with generic and union annotations and with some decorators combinations."""
    if res:
        assert f(x) == res
    else:
        with pytest.raises(TypeError):
            f(x)


@strict_typing
def gen(x: int, y):  # generator function returns an instance of <class 'generator'>
    for i in range(5):
        yield  i + x + y


@pytest.mark.parametrize('x, y, res', [
    (1, 2, 3),
    (1, 2.5, 3.5),
    (1.5, 2, None),
])
def test_gen(x, y, res):
    """Test with generator function."""
    if res:
        g = gen(x, y)
        assert next(g) == res
    else:
        with pytest.raises(TypeError):
            g = gen(x, y)


@strict_typing
async def cor(x: int, y):  # coroutine returns an instance of <class 'coroutine'>
    await asyncio.sleep(0)
    return x + y


@pytest.mark.parametrize('x, y, res', [
    (1, 2, 3),
    (1, 2.5, 3.5),
    (1.5, 2, None),
])
def test_async_def(x, y, res):
    """Test coroutine."""
    if res:
        assert asyncio.run(cor(x, y)) == res
    else:
        with pytest.raises(TypeError):
            cor(x, y)
