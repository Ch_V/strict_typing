import pytest

from strict_typing import strict_typing


@strict_typing()
class BP1:
    def f(self, x: int):
        return x

bp1 = BP1()


@strict_typing(enabled=False)
class BP2:
    def f(self, x: int):
        return x

bp2 = BP2()


@strict_typing(enabled=True)
class BP3:
    def f(self, x) -> int:
        return x

    class BP4:
        @staticmethod
        def fun(self):
            pass

bp3 = BP3()


@pytest.mark.parametrize('f, x, res', [
    (bp1.f, 2, 2),
    (bp1.f, 2.2, None),
    (bp2.f, 2, 2),
    (bp2.f, 2.2, 2.2),
    (bp3.f, 2, 2),
    (bp3.f, 2.2, None),
])
def test_enabled(f, x, res):
    """Test of `enabled` boolean kwarg work."""
    if res:
        assert f(x) == res
    else:
        with pytest.raises(TypeError):
            f(x)


def test_qualname():
    """Test `__qualname__` attribute of decorated class attributes."""
    assert bp1.f.__qualname__ == 'BP1.f'
    assert bp3.BP4.fun.__qualname__ == 'BP3.BP4.fun'


@strict_typing
class NST:
    def f(self, x):
        def g(x_: int):
            return x_
        return g(x)

    class NST_inner:
        @staticmethod
        def g(x: int):
            return x

nst = NST()


@pytest.mark.parametrize('f, x, res', [
    (nst.f, 5, 5),
    (nst.f, 5.5, None),
    (nst.NST_inner.g, 5, 5),
    (nst.NST_inner.g, 5.5, None),
])
def test_nesting(f, x, res):
    """Test `@strict_typing` affect on nested objects."""
    if res:
        assert f(x) == res
    else:
        with pytest.raises(TypeError):
            f(x)


@strict_typing
class CS:
    @classmethod
    def c(cls, x: int):
        return x

    @staticmethod
    def s(x: int):
        return x

cs = CS()


@pytest.mark.parametrize('f, x, res', [
    (cs.c, 5, 5),
    (cs.c, 5.5, None),
    (cs.s, 5, 5),
    (cs.s, 5.5, None),
])
def test_class_static(f, x, res):
    """Test with `@staticmethod` and `@classmethod` decorators."""
    if res:
        assert f(x) == res
    else:
        with pytest.raises(TypeError):
            f(x)


@strict_typing
class Pr:
    def __init__(self):
        self._x = 123

    @property
    def p(self):
        return self._x

    @p.setter
    def p(self, x: int):
        self._x = x

pr = Pr()


def test_prop():
    """Test with `@(property).setter` decorator."""
    assert pr.p == 123
    pr.p = 222
    assert pr.p == 222
    with pytest.raises(TypeError):
        pr.p = 5.5
